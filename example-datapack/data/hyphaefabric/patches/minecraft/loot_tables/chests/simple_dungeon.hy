# can also use pools[2].entries or pools.2["entries"], for example

pools.2.entries :~= {
  "type": "minecraft:item",
  "weight": 10,
  "functions": [
    {
      "function": "minecraft:set_count",
      "count": {
        "min": 1.0,
        "max": 4.0,
        "type": "minecraft:uniform"
      }
    }
  ],
  "name": "minecraft:egg"
}

