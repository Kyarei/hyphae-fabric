if startsWith?(idPath(hic()), "loot_tables/blocks/acacia_") {
  pools :~= {
    "rolls": 1,
    "entries": [
      {
        "type": "minecraft:item",
        "name": "minecraft:egg"
      }
    ]
  }
}

