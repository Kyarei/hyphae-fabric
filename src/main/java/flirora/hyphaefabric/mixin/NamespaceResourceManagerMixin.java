package flirora.hyphaefabric.mixin;

import flirora.hyphaefabric.impl.OverridableResourceManager;
import net.minecraft.resource.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.Pair;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mixin(NamespaceResourceManager.class)
public abstract class NamespaceResourceManagerMixin implements ResourceManager, OverridableResourceManager {
    @Shadow
    @Final
    protected List<ResourcePack> packList;

    @Shadow
    private void validate(Identifier id) throws IOException {
        throw new UnsupportedOperationException("this is a mixin class dum dum");
    }

    @Shadow
    static Identifier getMetadataPath(Identifier id) {
        return null;
    }

    @Shadow
    @Final
    private ResourceType type;

    @Shadow
    protected abstract InputStream open(Identifier id, ResourcePack pack) throws IOException;

    // Each value (i, r) specifies that r takes precedence over resources from
    // the first i resource packs registered.
    private final Map<Identifier, Pair<Integer, Resource>> overridenResources = new HashMap<>();

    @Override
    public void overrideResource(Identifier id, Resource resource) {
        overridenResources.put(id, new Pair<>(this.packList.size(), resource));
    }

    @Inject(method = "getResource", at = @At(value = "HEAD"),
            cancellable = true)
    private void onGetResource(Identifier id, CallbackInfoReturnable<Resource> cir) throws IOException {
        this.validate(id);
        ResourcePack lastResourcePack = null;
        Identifier metaId = getMetadataPath(id);
        Pair<Integer, Resource> overrideResource = overridenResources.get(id);

        for (int i = this.packList.size() - 1; i >= 0; --i) {
            if (overrideResource != null && i < overrideResource.getLeft()) {
                cir.setReturnValue(overrideResource.getRight());
                return;
            }

            ResourcePack current = this.packList.get(i);
            if (lastResourcePack == null && current.contains(this.type, metaId)) {
                lastResourcePack = current;
            }

            if (current.contains(this.type, id)) {
                InputStream inputStream = null;
                if (lastResourcePack != null) {
                    inputStream = this.open(metaId, lastResourcePack);
                }

                cir.setReturnValue(new ResourceImpl(current.getName(), id, this.open(id, current), inputStream));
                return;
            }
        }

        throw new FileNotFoundException(id.toString());
    }
}
