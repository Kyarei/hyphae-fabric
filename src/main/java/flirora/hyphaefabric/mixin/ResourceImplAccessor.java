package flirora.hyphaefabric.mixin;

import net.minecraft.resource.ResourceImpl;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Accessor;

import java.io.InputStream;

@Mixin(ResourceImpl.class)
public interface ResourceImplAccessor {
    @Accessor(value = "metaInputStream")
    InputStream getMetaInputStream();
}
