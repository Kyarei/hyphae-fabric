package flirora.hyphaefabric.mixin;

import flirora.hyphaefabric.impl.Discovery;
import flirora.hyphaefabric.impl.OverridableResourceManager;
import net.minecraft.resource.*;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import java.util.Map;

@Mixin(ReloadableResourceManagerImpl.class)
public abstract class ReloadableResourceManagerImplMixin implements ReloadableResourceManager, OverridableResourceManager {
    @Shadow
    @Final
    private ResourceType type;

    @Shadow
    @Final
    private Map<String, NamespaceResourceManager> namespaceManagers;

    @Inject(method = "addPack", at = @At("HEAD"))
    private void onAddPack(ResourcePack resourcePack, CallbackInfo ci) {
        Discovery.onAddResourcePack((ReloadableResourceManagerImpl) (Object) this, resourcePack, this.type);
    }

    @Override
    public void overrideResource(Identifier id, Resource resource) {
        ResourceManager resourceManager = this.namespaceManagers.get(id.getNamespace());
        if (resourceManager != null)
            ((OverridableResourceManager) resourceManager).overrideResource(id, resource);
    }
}
