package flirora.hyphaefabric.api

import net.minecraft.util.Identifier

case class UnobfuscatedIdentifier(id: Identifier) {
  implicit def toMinecraft: Identifier = id

  override def toString: String = id.toString
}

object UnobfuscatedIdentifier {
  implicit def fromMinecraft(id: Identifier): UnobfuscatedIdentifier =
    UnobfuscatedIdentifier(id)
}
