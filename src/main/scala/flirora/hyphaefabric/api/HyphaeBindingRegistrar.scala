package flirora.hyphaefabric.api

import flirora.hyphae.interpret.interop.{
  IntrinsicFunctionRegistry,
  MarshallerRegistry
}

trait HyphaeBindingRegistrar {
  def registerMarshallers(registry: MarshallerRegistry): Unit
  def registerIntrinsicFunctions(registry: IntrinsicFunctionRegistry,
                                 marshallers: MarshallerRegistry): Unit
}
