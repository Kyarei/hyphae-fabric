package flirora.hyphaefabric.impl.transcoder

import flirora.hyphae.parse.{StringValue, Value}
import flirora.hyphae.transcode.{Transcoder, TranscodingError}
import flirora.hyphaefabric.api.UnobfuscatedIdentifier
import net.minecraft.util.{Identifier, InvalidIdentifierException}

object IdentifierTranscoder extends Transcoder[UnobfuscatedIdentifier] {
  override def toValue(
    obj: UnobfuscatedIdentifier
  ): Either[TranscodingError, Value] =
    Right(StringValue(obj.toString))

  override def fromValue(
    value: Value
  ): Either[TranscodingError, UnobfuscatedIdentifier] =
    value match {
      case StringValue(s) =>
        try {
          Right(new Identifier(s))
        } catch {
          case ex: InvalidIdentifierException =>
            Left(TranscodingError(ex.getMessage))
        }
      case _ =>
        Left(
          TranscodingError(
            s"${value.getType} cannot be cast into an identifier"
          )
        )
    }

  override val typename: String = "identifier"
}
