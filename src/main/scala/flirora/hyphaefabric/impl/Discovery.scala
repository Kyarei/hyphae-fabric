package flirora.hyphaefabric.impl

import java.io.{BufferedReader, ByteArrayInputStream, InputStreamReader}
import java.nio.charset.StandardCharsets

import com.google.gson.{JsonParser, JsonSyntaxException}
import flirora.hyphae.Hyphae
import flirora.hyphae.parse.InterpreterError
import flirora.hyphae.transcode.GsonTranscoder
import flirora.hyphaefabric.HyphaeFabricMod
import flirora.hyphaefabric.api.UnobfuscatedIdentifier
import flirora.hyphaefabric.mixin.ResourceImplAccessor
import net.minecraft.resource._
import net.minecraft.util.Identifier
import org.apache.commons.io.IOUtils
import org.apache.logging.log4j.Level

import scala.jdk.CollectionConverters._
import scala.util.matching.Regex

case class DetectedPatcherEntry(patcher: Identifier, target: Identifier)

object Discovery {
  private def pathGlobPatternToRegex(path: String): Regex = {
    val parts = path.split('/')
    parts
      .map(s => {
        if (s == "__" || s.startsWith("__."))
          ".*" + Regex.quote(s.substring(2))
        else if (s == "_" || s.startsWith("_."))
          "[^/]*" + Regex.quote(s.substring(1))
        else Regex.quote(s)
      })
      .mkString("/")
      .r
  }

  def identifierGlob(resourceManager: ResourceManager,
                     identifier: Identifier): Iterable[Identifier] = {
    val namespaceRegex = pathGlobPatternToRegex(identifier.getNamespace)
    val pathRegex = pathGlobPatternToRegex(identifier.getPath)
    resourceManager
    // We can't use the pathPredicate argument here as it doesn't give the
    // full path
      .findResources("", _ => true)
      .asScala
      .filter(
        id =>
          namespaceRegex.matches(id.getNamespace) && pathRegex
            .matches(id.getPath)
      )
  }

  // Find patchers and return a list of their targets
  def findPatchers(
    resourcePack: ResourcePack,
    resourceType: ResourceType
  ): Iterable[DetectedPatcherEntry] = {
    for {
      namespace <- resourcePack
        .getNamespaces(resourceType)
        .asScala
      patch <- resourcePack
        .findResources(
          resourceType,
          namespace,
          "patches",
          Int.MaxValue,
          _.endsWith(".hy")
        )
        .asScala
        .map((id) => {
          val path = id.getPath.drop(id.getPath.indexOf('/') + 1)
          val idx = path.indexOf('/')
          val targetNamespace = path.take(idx)
          val targetPath = path.drop(idx + 1).dropRight(3) + ".json"
          DetectedPatcherEntry(id, new Identifier(targetNamespace, targetPath))
        })
    } yield patch
  }

  // Using JsonParser's static methods is the preferred way now, but Minecraft
  // uses an outdated version of GSON and we don't want to have to bundle
  // our own copy so we have to do it like this.
  private val PARSER = new JsonParser

  def applyPatcher(
    manager: ReloadableResourceManagerImpl,
    resourcePack: ResourcePack,
    resourceType: ResourceType,
    patcher: Identifier,
    targetGlob: Identifier
  ): Either[Iterable[InterpreterError], Unit] = {
    val source =
      IOUtils.toString(
        resourcePack.open(resourceType, patcher),
        StandardCharsets.UTF_8
      )
    val program = Hyphae.parse(source, None) match {
      case Left(err)  => return Left(err)
      case Right(res) => res
    }
    for (target <- identifierGlob(manager, targetGlob)) {
      println(s"patching $target...")
      val unpatchedResource = manager.getResource(target)
      val unpatchedJson = PARSER.parse(
        new BufferedReader(
          new InputStreamReader(unpatchedResource.getInputStream)
        )
      )
      val result = Hyphae.runOverTranscoded(
        GsonTranscoder,
        program,
        unpatchedJson,
        HyphaeFabricMod.getIntrinsicFunctionRegistry,
        None,
        trove => {
          trove.put(
            UnobfuscatedReloadableResourceManagerImpl(manager),
            allowOverwrites = true
          )
          trove.put(UnobfuscatedIdentifier(target), allowOverwrites = true)
        }
      ) match {
        case Left(err)  => return Left(List(err))
        case Right(res) => res
      }
      val resultStr = result.toString
      val patchedStream = new ByteArrayInputStream(
        resultStr.getBytes(StandardCharsets.UTF_8)
      )
      val patchedResource = new ResourceImpl(
        unpatchedResource.getResourcePackName,
        target,
        patchedStream,
        unpatchedResource
          .asInstanceOf[ResourceImplAccessor]
          .getMetaInputStream
      )
      manager
        .asInstanceOf[OverridableResourceManager]
        .overrideResource(target, patchedResource)
    }
    Right(())
  }

  def applyPatchers(entries: Iterable[DetectedPatcherEntry],
                    resourcePack: ResourcePack,
                    resourceType: ResourceType,
                    manager: ReloadableResourceManagerImpl): Int = {
    var errors = 0
    for (DetectedPatcherEntry(patcher, target) <- entries) {
      applyPatcher(manager, resourcePack, resourceType, patcher, target) match {
        case Left(err) =>
          errors += err.size
          if (err.size == 1)
            HyphaeFabricMod.log(
              Level.ERROR,
              s"Error in applying Hyphae script $patcher to $target: ${err.head.show}"
            )
          else
            HyphaeFabricMod.log(
              Level.ERROR,
              s"Errors in applying Hyphae script $patcher to $target:\n${err.mkString("\n")}"
            )
        case Right(_) => ()
      }
    }
    errors
  }

  def onAddResourcePack(manager: ReloadableResourceManagerImpl,
                        resourcePack: ResourcePack,
                        resourceType: ResourceType): Unit = {
    val patchers = findPatchers(resourcePack, resourceType)
    if (patchers.nonEmpty)
      HyphaeFabricMod.log(
        Level.INFO,
        s"${patchers.size} patchers found in ${resourcePack.getName}"
      )
    val errors = applyPatchers(patchers, resourcePack, resourceType, manager)
    if (errors != 0) {
      HyphaeFabricMod.log(Level.ERROR, s"$errors error(s) total")
      throw new JsonSyntaxException("Fix your scriptim!")
    }
  }
}
