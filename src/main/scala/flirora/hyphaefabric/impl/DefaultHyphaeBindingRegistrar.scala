package flirora.hyphaefabric.impl

import java.io.FileNotFoundException
import java.nio.charset.StandardCharsets

import com.google.gson.JsonParser
import flirora.hyphae.interpret.Environment
import flirora.hyphae.interpret.interop.{
  IntrinsicFunctionRegistry,
  MarshallerRegistry
}
import flirora.hyphae.parse.Value
import flirora.hyphae.transcode.GsonTranscoder
import flirora.hyphaefabric.api.{HyphaeBindingRegistrar, UnobfuscatedIdentifier}
import flirora.hyphaefabric.impl.transcoder.IdentifierTranscoder
import net.minecraft.util.Identifier
import org.apache.commons.io.IOUtils

class DefaultHyphaeBindingRegistrar extends HyphaeBindingRegistrar {
  override def registerMarshallers(registry: MarshallerRegistry): Unit = {
    registry.registerMarshallerWithOptional(IdentifierTranscoder)
  }

  override def registerIntrinsicFunctions(
    registry: IntrinsicFunctionRegistry,
    marshallers: MarshallerRegistry
  ): Unit = {
    registry.register(
      marshallers.wrapFunction(
        (s: String) => Identifier.tryParse(s) != null,
        "identifier?"
      )
    )
    registry.register(
      marshallers
        .wrapFunction(
          (id: UnobfuscatedIdentifier) => id.toMinecraft.getNamespace,
          "idNamespace"
        )
    )
    registry.register(
      marshallers
        .wrapFunction(
          (id: UnobfuscatedIdentifier) => id.toMinecraft.getPath,
          "idPath"
        )
    )
    registry.register(
      marshallers.wrapFunctionFallible(
        (s: String) =>
          GsonTranscoder
            .toValue(JsonParser.parseString(s))
            .left
            .map(err => s"Parsing failed: ${err.message}"),
        "parseJson"
      )
    )
    registry.register(
      marshallers.wrapFunctionFallible(
        (v: Value) =>
          GsonTranscoder
            .fromValue(v)
            .left
            .map(err => s"Stringifying failed: ${err.message}")
            .map(_.toString),
        "stringifyJson"
      )
    )
    // TODO: also support binary data -- would require a "byte string" data type
    // in Hyphae
    registry.register(
      marshallers.wrapFunctionEnvFallible(
        (environment: Environment, id: UnobfuscatedIdentifier) =>
          try {
            val trove = environment.trove
            trove.get[UnobfuscatedReloadableResourceManagerImpl] match {
              case Some(UnobfuscatedReloadableResourceManagerImpl(manager)) =>
                val stream = manager.getResource(id.toMinecraft).getInputStream
                val res = Right(
                  IOUtils
                    .toString(stream, StandardCharsets.UTF_8)
                )
                stream.reset()
                res
              case None => Left("could not find a resource manager")
            }
          } catch {
            case ex: FileNotFoundException => Left(ex.getMessage)
        },
        "resourceAt"
      )
    )

    registry.register(
      marshallers.wrapFunctionEnvFallible(
        env =>
          env.trove.get[UnobfuscatedIdentifier] match {
            case Some(s) => Right(s)
            case None    => Left("could not find id of current target")
        },
        "hic"
      )
    )
  }
}
