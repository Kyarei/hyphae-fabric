package flirora.hyphaefabric.impl

import net.minecraft.resource.Resource
import net.minecraft.util.Identifier

trait OverridableResourceManager {
  def overrideResource(id: Identifier, resource: Resource): Unit
}
