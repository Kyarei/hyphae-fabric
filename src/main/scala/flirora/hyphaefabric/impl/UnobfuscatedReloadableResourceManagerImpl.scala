package flirora.hyphaefabric.impl

import net.minecraft.resource.ReloadableResourceManagerImpl

case class UnobfuscatedReloadableResourceManagerImpl(
  manager: ReloadableResourceManagerImpl
)
