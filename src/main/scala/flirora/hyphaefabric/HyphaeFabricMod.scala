package flirora.hyphaefabric

import flirora.hyphae.interpret.interop.{
  IntrinsicFunctionRegistry,
  MarshallerRegistry
}
import flirora.hyphaefabric.api.HyphaeBindingRegistrar
import net.fabricmc.api.ModInitializer
import net.fabricmc.loader.api.FabricLoader
import org.apache.logging.log4j.{Level, LogManager, Logger}

import scala.jdk.CollectionConverters._

object HyphaeFabricMod extends ModInitializer {
  val LOGGER: Logger = LogManager.getLogger()
  val MOD_ID: String = "hyphaefabric"
  val MOD_NAME: String = "Hyphae for Fabric"

  private lazy val INTRINSIC_FUNCTION_REGISTRY = {
    val entryPoints = FabricLoader
      .getInstance()
      .getEntrypoints(
        "hyphaefabric:binding_registrar",
        classOf[HyphaeBindingRegistrar]
      )
      .asScala
    System.err.println(s"Loading ${entryPoints.size} binding registrar(s)")
    val marshallerRegistry = MarshallerRegistry.createStandard
    for (ep <- entryPoints) {
      ep.registerMarshallers(marshallerRegistry)
    }
    val intrinsicFunctionRegistry =
      IntrinsicFunctionRegistry.createStandard(marshallerRegistry)
    for (ep <- entryPoints) {
      ep.registerIntrinsicFunctions(
        intrinsicFunctionRegistry,
        marshallerRegistry
      )
    }
    intrinsicFunctionRegistry
  }

  def getIntrinsicFunctionRegistry: IntrinsicFunctionRegistry =
    INTRINSIC_FUNCTION_REGISTRY

  def log(level: Level, message: String): Unit =
    LOGGER.log(level, s"[$MOD_NAME] $message")

  override def onInitialize(): Unit = {
    HyphaeFabricMod.log(Level.INFO, "Initializing")
    val functionRegistry = HyphaeFabricMod.getIntrinsicFunctionRegistry
    val functionNames = Seq.from(functionRegistry.listFunctions.keys).sorted
    HyphaeFabricMod.log(
      Level.INFO,
      s"Available built-in functions: ${functionNames.mkString(", ")}"
    )
  }
}
