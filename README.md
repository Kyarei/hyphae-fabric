## hyphae-fabric

Fabric bindings for [Hyphae](https://gitlab.com/Kyarei/hyphae).

Tired of having to completely override loot tables when all you wanted was to add an entry? Tired of having datapacks conflict with each other? **hyphae-fabric** is here to help. To patch a JSON file, put it in the `{assets|data}/<your namespace>/patches/<target namespace>/<target path>.json`. For example, if you wanted to tweak the loot table for the dungeon chest, then create the file `data/<your namespace>/patches/minecraft/loot_tables/chests/simple_dungeon.hy`. You can glob a path part with `_` (with possibly a `.json` file extension, or a recursive path part with `__`. See `example-datapack/` to see what you can do.

### Registering your own marshallers and built-in functions

Create an entry point extending `flirora.hyphaefabric.impl.HyphaeBindingRegistrar` and list in in `fabric.mod.json` under `hyphaefabric:binding_registrar`.

Note that it is generally a Bad Idea™ to register transcoders for obfuscated classes. You can register transcoders for wrappers around such classes (see `UnobfuscatedIdentifier` for reference).

### Built-in functions added by hyphae-fabric

* Functions that work with identifiers (taken as strings): `identifier?`, `idNamespace`, `idPath`
* Parse and stringify JSON: `parseJson`, `stringifyJson`
* Get the contents of the resource (takes an identifier as a string): `resourceAt`
* Identifier of the data being patched: `hic`

This mod bundles the [Scala Parser Combinators](https://github.com/scala/scala-parser-combinators) library, licensed under the Apache-2.0 license, which Hyphae uses. Hyphae itself and this mod are licensed under the MIT license.

